import { Component, Input, OnInit, SimpleChanges } from '@angular/core';

import { Game } from '../game';
import { Team } from '../team';
import { DataService } from '../data.service';

@Component({
  selector: 'app-future-games',
  templateUrl: './future-games.component.html',
  styleUrls: ['./future-games.component.css']
})
export class FutureGamesComponent implements OnInit {

  futureGames!: Game[];
  @Input() team!: Team;

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['team']) {
      this.getFutureGames();
    }
  }

  getFutureGames(): void {
    this.dataService.getGames().subscribe(temp => { 
      var tempArr: Game[] = [];
      var count: number = 0;
      temp.forEach(element => {
        if((element.hteamid == this.team.id || element.ateamid == this.team.id) && element.complete == 0 && count < 10) {
          tempArr.push(element);
          count += 1;
        }
      });
      
      this.futureGames = tempArr;    
    });
  }

}
