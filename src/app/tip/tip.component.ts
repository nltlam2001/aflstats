import { Component, OnInit, Input, SimpleChanges } from '@angular/core';

import { Tip } from '../tip';
import { DataService } from '../data.service';
import { Team } from '../team';

@Component({
  selector: 'app-tip',
  templateUrl: './tip.component.html',
  styleUrls: ['./tip.component.css']
})
export class TipComponent implements OnInit {
  tip!: Tip;
  @Input() team!: Team;

  constructor(private dataService : DataService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['team']) {
      this.getTip();
    }
  }
  
  getTip(): void {
    this.dataService.getTips().subscribe(temp => {
      temp.forEach( element => {
        if (element.tipteamid == this.team.id) {
          this.tip = element; 
        }
      });
    });
  }

}
