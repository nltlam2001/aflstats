import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GamesVsRivalComponent } from './games-vs-rival.component';

describe('GamesVsRivalComponent', () => {
  let component: GamesVsRivalComponent;
  let fixture: ComponentFixture<GamesVsRivalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GamesVsRivalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GamesVsRivalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
