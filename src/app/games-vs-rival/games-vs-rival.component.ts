import { Component, OnInit, Input, SimpleChanges } from '@angular/core';

import { Game } from '../game';
import { Team } from '../team';
import { DataService } from '../data.service';
@Component({
  selector: 'app-games-vs-rival',
  templateUrl: './games-vs-rival.component.html',
  styleUrls: ['./games-vs-rival.component.css']
})
export class GamesVsRivalComponent implements OnInit {

  gamesVsRival: Game[] = []; 
  @Input() team!: Team;
  @Input() rival!: Team;
  @Input() year!: number;

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.getGamesVsRival();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['team']) {
      this.getGamesVsRival();
    }
    if (changes['year']) {
      this.getGamesVsRival();
    }
    if (changes['rival']) {
      this.getGamesVsRival();
    }
  }

  getGamesVsRival(): void {
    this.dataService.getGames().subscribe(temp => { 
      var tempArr: Game[] = [];
      temp.forEach(element => {
        if(((element.hteamid == this.team.id && element.ateamid == this.rival.id) || (element.ateamid == this.team.id && element.hteamid == this.rival.id)) && element.complete == 100 && element.year == this.year ) {
          tempArr.push(element);
        }
      });
      
      this.gamesVsRival = tempArr;    
    });
  }
}
