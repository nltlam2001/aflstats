import { Component, Input, OnInit, SimpleChanges } from '@angular/core';

import { Game } from '../game';
import { Team } from '../team';
import { DataService } from '../data.service';
import { elementEventFullName } from '@angular/compiler/src/view_compiler/view_compiler';

@Component({
  selector: 'app-team-result',
  templateUrl: './team-result.component.html',
  styleUrls: ['./team-result.component.css']
})
export class TeamResultComponent implements OnInit {

  gamesPlayed!: Game[];
  @Input() team!: Team;
  @Input() year!: number;

  constructor(private dataService: DataService) { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['team']) {
      this.getGamesPlayed();
    }
    if (changes['year']) {
      this.getGamesPlayed();
    }
  }

  ngOnInit(): void {
  }

  getGamesPlayed(): void {
    this.dataService.getGames().subscribe(temp => { 
      var tempArr: Game[] = [];

      temp.forEach(element => {
        if((element.hteamid == this.team.id || element.ateamid == this.team.id) && element.year == this.year && element.complete==100) tempArr.push(element);
      });
      
      this.gamesPlayed = tempArr;    
    });
  }
}
