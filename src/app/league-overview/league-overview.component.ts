import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Team } from '../team';
import { Game } from '../game';

@Component({
  selector: 'app-league-overview',
  templateUrl: './league-overview.component.html',
  styleUrls: ['./league-overview.component.css']
})
export class LeagueOverviewComponent implements OnInit {

  teams!: Team[];
  games!: Game[];

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.getTeams();
    this.getGames();
  }

  getTeams(): void {
    this.dataService.getTeams().subscribe(temp => { this.teams = temp; });
  }

  getGames(): void {
    this.dataService.getGames().subscribe(temp => { 
      var tempArr: Game[] = [];
      var count: number = 0;

      // loop through the raw data array to find games where the home team won
      // logic: hteam == winner from the Game model we get from the transformed API data
      
      temp.forEach(element => {
        if(element.hteam == element.winner && count < 50 && element.year==2021) {
          tempArr.push(element);
          count += 1;
        }
      });
      
      this.games = tempArr;  

    });
  }

}
