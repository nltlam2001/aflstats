import { Component, OnInit } from '@angular/core';

import { Team } from '../team';
import { DataService } from '../data.service';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {
  
  teams!: Team[];
  selectedTeam!: Team;
  choice!: string;
  rival!: Team;
  hide: boolean= true;
  rivalSelect: boolean= true;
  tipHide: boolean= false;
  gamesPlayedHide: boolean= false;
  futureGamesHide: boolean= false;  
  venuesHide: boolean=false;
  gamesVsRivalHide: boolean=false;
  year: number = 2021;

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.getAFLTeams();
  }

  onSelect(team: Team): void {
    this.selectedTeam = team;
    this.hide = false;
  }

  onSelectRival(team: Team): void {
    this.rival = team;
    this.rivalSelect = false;
  }

  onSelectComponent(choice: string): void {
    this.choice = choice;
    this.resetChoice(choice);
  }

  onShow(): void {
    this.hide=true;
  }

  onShowRival(): void {
    this.rivalSelect=true;
  }

  onSelectYear(year: number): void {
    this.year = year;
  }

  resetChoice(choice: string): void {
    switch (choice) {
      case "Tip":
        this.tipHide = true;
        this.gamesPlayedHide = false;
        this.futureGamesHide = false;
        this.venuesHide = false;
        this.gamesVsRivalHide = false;
        break;
      case "Games History":
        this.tipHide = false;
        this.gamesPlayedHide = true;
        this.futureGamesHide = false;
        this.venuesHide = false;
        this.gamesVsRivalHide = false;
        break;
      case "Future Games":
        this.tipHide = false;
        this.gamesPlayedHide = false;
        this.futureGamesHide = true;
        this.venuesHide = false;
        this.gamesVsRivalHide = false;
        break;
      case "Venues":
        this.tipHide = false;
        this.gamesPlayedHide = false;
        this.futureGamesHide = false;
        this.venuesHide = true;
        this.gamesVsRivalHide = false;
        break;
      case "Games vs Rival":
        this.tipHide = false;
        this.gamesPlayedHide = false;
        this.futureGamesHide = false;
        this.venuesHide = false;
        this.gamesVsRivalHide = true;
        break;
    }
  }

  getAFLTeams(): void {
    this.dataService.getTeams().subscribe(temp => { this.teams = temp; });
  }
}
