import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LeagueOverviewComponent } from './league-overview/league-overview.component';
import { TeamComponent } from './team/team.component';

const routes: Routes = [
  { path: 'LeagueOverview', component: LeagueOverviewComponent},
  { path: 'Team', component: TeamComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
