import { Component, Input, OnInit, SimpleChanges } from '@angular/core';

import { Game } from '../game';
import { Team } from '../team';
import { DataService } from '../data.service';

@Component({
  selector: 'app-venue',
  templateUrl: './venue.component.html',
  styleUrls: ['./venue.component.css']
})
export class VenueComponent implements OnInit {
  venues!: string[];
  @Input() team!: Team;
  @Input() year!: number;

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.getVenues();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['team']) {
      this.getVenues();
    }
    if (changes['year']) {
      this.getVenues();
    }
  }

  getVenues(): void {
    this.dataService.getGames().subscribe(temp => { 
      var tempArr: string[] = [];
      temp.forEach(element => {
        if(element.winnerteamid == this.team.id && element.year == this.year && tempArr.indexOf(element.venue) < 0) {
          tempArr.push(element.venue);
        }
      });
      
      this.venues = tempArr;    
    });
  }

}
