import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule} from '@angular/common/http';
import { TeamComponent } from './team/team.component';
import { TeamResultComponent } from './team-result/team-result.component';
import { TipComponent } from './tip/tip.component';
import { GamesVsRivalComponent } from './games-vs-rival/games-vs-rival.component';
import { VenueComponent } from './venue/venue.component';
import { FutureGamesComponent } from './future-games/future-games.component';
import { LeagueOverviewComponent } from './league-overview/league-overview.component';


@NgModule({
  declarations: [
    AppComponent,
    TeamComponent,
    TeamResultComponent,
    TipComponent,
    GamesVsRivalComponent,
    VenueComponent,
    FutureGamesComponent,
    LeagueOverviewComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
