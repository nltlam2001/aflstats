export class Ranking {
    constructor(
        public name: string,
        public against: number,
        public behinds_against: number,
        public behinds_for: number,
        public draws: number,
        public fors: number,
        public goals_against: number,
        public goals_for: number,
        public id: number,
        public losses: number,
        public percentage: number,
        public played: number,
        public pts: number,
        public rank: number,
        public wins: number,
    ){}
}
